#define TrackFit_cxx
#include "TrackFit.h"
#include "TImage.h"
#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TMatrix.h>
#include <TMatrixD.h>
#include <TSystem.h>
#include <TNtuple.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <unordered_map>
#include "trackselect.h"
#include "POStransform.h"
#include "constants.h"
using namespace std;

const Int_t npts = 6; // chip nums
// 1 for chi2 fit and 2 for likelyhood fit

double xx[npts], yy[npts], zz[npts]; 
vector<double> tx,ty,tz;
int plane[npts];
double sigmax=0.025/pow(12,0.5), sigmay=0.025/pow(12,0.5); 
double limitX = 0.025;
double limitY = 0.025;

vector<Int_t> chipID_order = {1,2,3,4,5,6};//order-ID
vector<Int_t> chipID_order_contrast = {-1,0,1,2,3,4,5};//ID-order
unordered_map<int,double> zmap={{1,20},{2,60},{3,100},{4,140},{5,180},{6,220}};//ID-z
vector<pair<double,double>> hitwindow = {{0,0},{25.6,12.8}};
vector<double> Chicut = {10,5,2};
vector<double> cripro = {1,2,3};
unordered_map<int,vector<double>> align_par;//ID-par



void fcn_chisq(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  const Double_t a1 = par[0];
  const Double_t b1 = par[1];
  const Double_t a2 = par[2];
  const Double_t b2 = par[3];
  
  Double_t chisq=0.; 
  for( int i = 0; i < tz.size() ; i++ ) {
    Double_t chi1  = ( a1*tz[i] + b1 - tx[i] )/sigmax; chi1 = chi1*chi1;   
    Double_t chi2  = ( a2*tz[i] + b2 - ty[i] )/sigmay; chi2 = chi2*chi2;
    //Double_t chi1  = ( a1*zz[i] + b1 - xx[i] )/sigmax; chi1 = chi1*chi1;   
    //Double_t chi2  = ( a2*zz[i] + b2 - yy[i] )/sigmay; chi2 = chi2*chi2;    
    chisq += chi1 +chi2; 
  }	
  
  f = chisq;
}

// initialization and setup 

void Fit(Double_t results[])
{
  TMinuit *gMinuit = new TMinuit(4);  //initialize TMinuit with a maximum of 3 params
  gMinuit->SetFCN(fcn_chisq );
	
  Int_t    ierflg = 0;
  Double_t arglist[10], val[10], err[10];
	
  arglist[0] = -1;
  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);
  arglist[0] =0.5;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
	
  // Set starting values and step sizes for parameters
  static Double_t vstart[4] = { 0.0010, 1.,  0.0010, 1.};
  static Double_t   step[4] = { 0.0001, 0.1,  0.0001, 0.1};

  gMinuit->mnparm(0, "a1", vstart[0], step[0], 0, 0, ierflg);
  gMinuit->mnparm(1, "b1", vstart[1], step[1], 0, 0, ierflg);
  gMinuit->mnparm(2, "a2", vstart[2], step[2], 0, 0, ierflg);
  gMinuit->mnparm(3, "b2", vstart[3], step[3], 0, 0, ierflg);

  // Now ready for minimization step
  arglist[0] = 5000;
  arglist[1] = 1.00;
  gMinuit->mnexcm("MIGRAD", arglist , 1, ierflg); // mimimization here ... 
  //gMinuit->mnexcm("MINOS" , arglist , 2, ierflg); // Minos error 
	
  // Print results
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  //std::cout<<"nparx="<<nparx<<std::endl;

  TString chnam;
  Double_t xlolim, xuplim;
  Int_t iuext, iuint;
	
  for(Int_t p = 0; p < 4; p++)
  { 
    results[p] =0.0; 
    gMinuit->mnpout(p, chnam, val[p], err[p], xlolim, xuplim, iuint);
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p]*1000, err[p]*1000); 
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p], err[p]);
    results[p] = val[p]; 
  }
  delete gMinuit; 
}

void TrackFit::Loop(TFile * f, TTree* myTree,string path,string OPTION)
{  
  
   f->cd();
   double Chi2_Out, Chi2_OutY, Chi2_OutX;
   myTree->Branch("Chi2", &Chi2_Out);

   myTree->Branch("Chi2X", &Chi2_OutX);

   myTree->Branch("Chi2Y", &Chi2_OutY);

   vector<int>* vec_planeOut = new std::vector<int>();
   myTree->Branch("planeID", &vec_planeOut);

   vector<double>* vec_xlocOut = new std::vector<double>();
   myTree->Branch("xloc", &vec_xlocOut);

   vector<double>* vec_ylocOut = new std::vector<double>();
   myTree->Branch("yloc", &vec_ylocOut);
   TH1D * distance[npts][2];
   TH1D * res[npts];


   if(OPTION == "origin"){
    Chicut[0] = 15000;
    Chicut[1] = 10000;
    Chicut[2] =  5000;
   }

   for (int m=0; m < npts; m++){
     TString nameX = "chi2_"+to_string(m);
     res[m] = new TH1D(nameX, "", 100, 0, 200);
   }

   for (int m=0; m < npts; m++){
     TString nameX = "distance"+to_string(m)+"_x";
     TString nameY = "distance"+to_string(m)+"_y";

     distance[m][0] = new TH1D(nameX, "", 100, -0.15, 0.5);
     distance[m][1] = new TH1D(nameY, "", 100, -0.15, 0.5);
   }
  
   Long64_t nentries = fChain->GetEntriesFast();
   double residualX;
   double residualY;
   Double_t results[4]; 
   
   unordered_map<int,vector<double>> minus_eff = {{Chicut[0],vector<double>(6,0)},{Chicut[1],vector<double>(6,0)},{Chicut[2],vector<double>(6,0)}};
   unordered_map<int,vector<double>> total_eff = {{Chicut[0],vector<double>(6,0)},{Chicut[1],vector<double>(6,0)},{Chicut[2],vector<double>(6,0)}};

   
   trackselect* ts = new trackselect();
   POStransform  * trf = new POStransform();
   int chipnums = 0;
   std::vector<double> vec_chi2;
   std::vector<double> vec_chi2X;
   std::vector<double> vec_chi2Y;
   std::vector<double> vec_a1;
   std::vector<double> vec_a2;
   std::vector<double> vec_b1;
   std::vector<double> vec_b2;
   std::vector<double> ttx;
   std::vector<double> tty;
   std::vector<double> ttz;
   vector<vector<int>> chip_order;

   
   cout<<"Begin"<<endl;
   
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      fChain->GetEntry(jentry);   
      //if(numOfhits!=npts&&numOfhitchips==npts) continue;
      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();  
     
      if(jentry%10000 == 0) std::cout<<"Processing event " <<jentry<<"..."<<std::endl;

      vec_chi2.clear();
      vec_chi2X.clear();
      vec_chi2Y.clear();
      vec_a1.clear();
      vec_b1.clear();
      vec_a2.clear();
      vec_b2.clear();
      chip_order.clear();

      ts->generate(*planeID);
      
      chip_order.assign(ts->getorder().begin(),ts->getorder().end());
      chipnums = ts->getchipnums();
      if(chipnums<=5) continue;
      for (int i = 0; i < chip_order.size(); i++)
      {
        double chi2 = 0,chi2x=0,chi2y=0;
        tx.clear();
        ty.clear();
        tz.clear();
        for (int j = 0; j < chipnums; j++)
        {
          plane[j] = planeID->at(chip_order[i][j]);
          trf->Fill(align_par[plane[j]],xloc->at(chip_order[i][j]),yloc->at(chip_order[i][j]),0);
          if(OPTION == "origin"){
            tx.push_back(xloc->at(chip_order[i][j]));
	          ty.push_back(yloc->at(chip_order[i][j]));
            tz.push_back(zmap[chipID_order_contrast[plane[j]]]);
            xx[j] = xloc->at(chip_order[i][j]);
            yy[j] = yloc->at(chip_order[i][j]);
            zz[j] = zmap[chipID_order_contrast[plane[j]]];
          }
          else{
            tx.push_back(trf->GetXe());
            ty.push_back(trf->GetYe());
            tz.push_back(trf->GetZe());
            xx[j] = trf->GetXe();
            yy[j] = trf->GetYe();
            zz[j] = trf->GetZe();
          }
          
          //cout<<xloc->at(chip_order[i][j])<<"\t"<<trf->GetX()<<"\t";
          //cout<<yloc->at(chip_order[i][j])<<"\t"<<trf->GetY()<<"\t";
          //cout<<zmap[plane[j]]<<"\t"<<trf->GetZ()<<endl;;
          //cout<<xx[j]<<"\t";
          //cout<<yy[j]<<"\t";
          //cout<<zz[j]<<endl;
        } 
        //cout<<endl;
        Fit(results);
        for (int k = 0; k < chipnums; k++)
        {
          //double dx = results[0]*tz[k] + results[1]-tx[k];
          //double dy = results[2]*tz[k] + results[3]-ty[k];
          double dx = results[0]*zz[k] + results[1]-xx[k];
          double dy = results[2]*zz[k] + results[3]-yy[k];
	        chi2 += (dx * dx) / (sigmax*sigmax) + (dy * dy) / (sigmay*sigmay);
          chi2x += (dx * dx) / (sigmax*sigmax);
          chi2y += (dy * dy) / (sigmay*sigmay);
        }
        vec_chi2.push_back(chi2);
        vec_chi2X.push_back(chi2x);
        vec_chi2Y.push_back(chi2y);
        vec_a1.push_back(results[0]);
        vec_b1.push_back(results[1]);
        vec_a2.push_back(results[2]);
        vec_b2.push_back(results[3]);
      }
      // for (int i = 0; i < TrackSize; i++) std::cout << "vec_chi2 " << i << " = " << vec_chi2[i]<< std::endl;
      double minChi2 = *min_element(vec_chi2.begin(), vec_chi2.end());
      int minPos = min_element(vec_chi2.begin(), vec_chi2.end()) - vec_chi2.begin();

      double minChi2x = *min_element(vec_chi2X.begin(), vec_chi2X.end());
      int minPosx = min_element(vec_chi2X.begin(), vec_chi2X.end()) - vec_chi2X.begin();

      double minChi2y = *min_element(vec_chi2Y.begin(), vec_chi2Y.end());
      int minPosy = min_element(vec_chi2Y.begin(), vec_chi2Y.end()) - vec_chi2Y.begin();
     
      //std::cout << "minChi2 = " << minChi2 << std::endl;
      //std::cout << "minPos = " << minPos << std::endl;


      if (minChi2 > 15000) continue;
      //if (minPos != minPosx || minPos != minPosy) continue;
      //if (OPTION !="origin"&&minChi2>50) continue;
      //if (minChi2<1000) cout<<minChi2<<endl;
      //eff
      ttx.clear();
      tty.clear();
      ttz.clear();
      for (int l = 0; l < chipnums; l++){
        trf->Fill(align_par[planeID->at(chip_order[minPos][l])],xloc->at(chip_order[minPos][l]),yloc->at(chip_order[minPos][l]),0);
        if(OPTION == "origin"){
          ttx.push_back(xloc->at(chip_order[minPos][l]));
          tty.push_back(yloc->at(chip_order[minPos][l]));
          ttz.push_back(zmap[planeID->at(chip_order[minPos][l])]);
        }else{
          ttx.push_back(trf->GetXe());
          tty.push_back(trf->GetYe());
          ttz.push_back(trf->GetZe());
        }
      }

      for (int l = 0; l < chipnums; l++)
      { 
        tx.clear();
        ty.clear();
        tz.clear();

        for(int s=0;s<chipnums;s++){
          if(s==l) continue;
          tx.push_back(ttx[s]);
          ty.push_back(tty[s]);
          tz.push_back(ttz[s]);
        }

        Fit(results);
        double tcut=0,tcx=1,tcy=1;
        double tdx=0,tdy=0;
        for (int s = 0; s < chipnums; s++)
        { 
          if(s==l) continue;
          double dx = results[0]*ttz[s] + results[1]-ttx[s];
          double dy = results[2]*ttz[s] + results[3]-tty[s];
	        tcut += (dx * dx) / (sigmax*sigmax) + (dy * dy) / (sigmay*sigmay);
        }
        res[l]->Fill(tcut);
        trf->Fill(align_par[planeID->at(chip_order[minPos][l])],xloc->at(chip_order[minPos][l]),yloc->at(chip_order[minPos][l]),0);

        if(OPTION == "origin"){
          residualX = results[0]*zmap[planeID->at(chip_order[minPos][l])] + results[1] -xloc->at(chip_order[minPos][l]) ;
          residualY = results[2]*zmap[planeID->at(chip_order[minPos][l])] + results[2] -yloc->at(chip_order[minPos][l]) ;
        }else{
          residualX = results[0]*(trf->GetZe()) + results[1] - trf->GetXe();
          residualY = results[2]*(trf->GetZe()) + results[3] - trf->GetYe();
        }

        //if(tcut<Chicut[0]) total_eff[Chicut[0]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        //if(tcut<Chicut[0]) total_eff[Chicut[1]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        //if(tcut<Chicut[0]) total_eff[Chicut[2]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;

        if(l==0){
          tcx = 2.5;
          tcy = 2.5;
        }else if(l==chipnums-1){
          tcx = 2.7;
          tcy = 2.7;
        }else{
          tcx = 1;
          tcy = 1;
        }
        //if(residualX*residualX>limitX*limitX*tcx*tcx||residualY*residualY>limitY*limitY*tcy*tcy){
        //  if(tcut<Chicut[0]) minus_eff[Chicut[0]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        //  if(tcut<Chicut[1]) minus_eff[Chicut[1]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        //  if(tcut<Chicut[2]) minus_eff[Chicut[2]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        //}
        tdx = residualX*residualX;
        tdy = residualY*residualY;
        if(tcut<Chicut[2]){
            total_eff[Chicut[2]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
            if(tdx>limitX*limitX*tcx*tcx*cripro[0]||tdy>limitY*limitY*tcy*tcy*cripro[0]) minus_eff[Chicut[2]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        }
        if(tcut<Chicut[1]){
            total_eff[Chicut[1]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
            if(tdx>limitX*limitX*tcx*tcx*cripro[0]||tdy>limitY*limitY*tcy*tcy*cripro[0]) minus_eff[Chicut[1]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        }
        if(tcut<Chicut[0]){
            total_eff[Chicut[0]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
            if(tdx>limitX*limitX*tcx*tcx*cripro[0]||tdy>limitY*limitY*tcy*tcy*cripro[0]) minus_eff[Chicut[0]][chipID_order_contrast[planeID->at(chip_order[minPos][l])]]++;
        }

        distance[l][0]->Fill(residualX);
        distance[l][1]->Fill(residualY);

      }
      //cout<<total_eff[Chicut[2]][0]<<" "<<minus_eff[Chicut[1]][0]<<endl;
      //cout<<total_eff[Chicut[2]][1]<<" "<<minus_eff[Chicut[1]][1]<<endl;
      //cout<<total_eff[Chicut[2]][2]<<" "<<minus_eff[Chicut[1]][2]<<endl;
      //cout<<total_eff[Chicut[2]][3]<<" "<<minus_eff[Chicut[1]][3]<<endl;
      //cout<<total_eff[Chicut[2]][4]<<" "<<minus_eff[Chicut[1]][4]<<endl;
      //cout<<total_eff[Chicut[2]][5]<<" "<<minus_eff[Chicut[1]][5]<<endl;
      //cout<<endl;

      //find track
      //if (chipnums!=npts) continue;
      if (numOfclusters!=npts) continue;
      //if (minChi2>Chicut[0]) continue;
      Chi2_Out = minChi2;
      Chi2_OutX = minChi2x;
      Chi2_OutY = minChi2y;
      for (int l = 0; l < npts; l++)
      {

        vec_planeOut->push_back(planeID->at(chip_order[minPos][l]));
        vec_xlocOut->push_back(xloc->at(chip_order[minPos][l]));
        vec_ylocOut->push_back(yloc->at(chip_order[minPos][l]));
       
      }
     myTree->Fill();
     
   }
   myTree->Write();
    
   delete ts;
   delete trf;
  
   TCanvas *canvas1 = new TCanvas("residualX", "residualX", 1920, 1080);
   canvas1->Divide(2, 3);
   for (int j=0; j<npts; j++)
   {
      canvas1->cd(j+1);
      distance[j][0] -> Fit("gaus", "Q");
      distance[j][0]->Draw();
   }
   canvas1->Write();

   TCanvas *canvas2 = new TCanvas("residualY", "residualY", 1920, 1080);
   canvas2->Divide(2, 3);
   for (int j=0; j<npts; j++)
   {
      canvas2->cd(j+1);
      distance[j][1] -> Fit("gaus", "Q");
      distance[j][1]->Draw();
   }
   canvas2->Write();
   
   TCanvas *canvas3 = new TCanvas("Chi2", "Chi2", 1920, 1080);
   canvas3->Divide(2, 3);
   for (int j=0; j<npts; j++)
   {
      canvas3->cd(j+1);
      res[j]->Draw();
   }
   canvas3->Write();
   //TImage* img = TImage::Create();
   //img->FromPad(canvas1);
   //img->WriteImage("resX.png");
   //img->FromPad(canvas2);
   //img->WriteImage("resY.png");
   //img->FromPad(canvas3);
   //img->WriteImage("Chi2.png");
   cout<<"end fit"<<endl;
    map<int,double> eff1={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};
    map<int,double> eff2={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};
    map<int,double> eff3={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};

    map<int,double> err1={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};
    map<int,double> err2={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};
    map<int,double> err3={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0}};

    for(int i=0;i<npts;i++){
      if(total_eff[Chicut[0]][i]!=0)  eff1[chipID_order[i]] = 1-minus_eff[Chicut[0]][i]/total_eff[Chicut[0]][i];
      if(total_eff[Chicut[1]][i]!=0)  eff2[chipID_order[i]] = 1-minus_eff[Chicut[1]][i]/total_eff[Chicut[1]][i];
      if(total_eff[Chicut[2]][i]!=0)  eff3[chipID_order[i]] = 1-minus_eff[Chicut[2]][i]/total_eff[Chicut[2]][i];

      err1[chipID_order[i]] = pow(eff1[chipID_order[i]]*(1-eff1[chipID_order[i]])/total_eff[Chicut[0]][i],0.5);
      err2[chipID_order[i]] = pow(eff2[chipID_order[i]]*(1-eff2[chipID_order[i]])/total_eff[Chicut[1]][i],0.5);
      err3[chipID_order[i]] = pow(eff3[chipID_order[i]]*(1-eff3[chipID_order[i]])/total_eff[Chicut[2]][i],0.5);      
    }
    string sig = "+-";
    ofstream dataFile;
    string optxt = path+".txt";
    dataFile.open(optxt,ofstream::out);
    for(int i=0;i<npts;i++){
        dataFile<<chipID_order[i]<<"\t";
    }
    dataFile<<endl;
    for(int i=0;i<npts;i++){
      if(i==npts-1) dataFile<<eff1[chipID_order[i]]<<sig<<err1[chipID_order[i]];
      else dataFile<<eff1[chipID_order[i]]<<sig<<err1[chipID_order[i]]<<"\t";
    }
    dataFile<<endl;
    for(int i=0;i<npts;i++){
      if(i==npts-1) dataFile<<eff2[chipID_order[i]]<<sig<<err2[chipID_order[i]];
      else dataFile<<eff2[chipID_order[i]]<<sig<<err2[chipID_order[i]]<<"\t";
    }
    dataFile<<endl;
    for(int i=0;i<npts;i++){
      if(i==npts-1) dataFile<<eff3[chipID_order[i]]<<sig<<err3[chipID_order[i]];
      else dataFile<<eff3[chipID_order[i]]<<sig<<err3[chipID_order[i]]<<"\t";
    }
    dataFile<<endl;
    //for(int i=0;i<npts;i++){
    //  if(i==npts-1) dataFile<<total_eff[Chicut[0]][i];
    //  else dataFile<<total_eff[Chicut[0]][i]<<"\t";
    //}
    dataFile<<endl;
    dataFile.close();
  
   //f->Close(); 
   
}

int main(int argc, char** argv)
{ 
  std::vector<std::string> args;
  std::copy(argv + 1, argv + argc, std::back_inserter(args));
  string direct = "/publicfs/atlas/atlasnew/higgs/hgg/huyiming/Testbeam_Dec_2022/";
  string runID  = "Run"+args[0];
  string strln  = "0_128.root";
  string path   = direct+runID+"/EFF";
  string strln1;
  if(args[1]=="origin"){
    strln1 = direct+runID+"/origin_test"+strln;
  }else{
    strln1 = direct+runID+"/align_test"+strln;
  }
  TFile * f = new TFile(strln1.c_str(), "RECREATE");
  TTree* myTree = new TTree("TrackInfo", "Selected tracks with minimum Chi2");

  string input1 =   "/publicfs/atlas/atlasnew/higgs/hgg/huyiming/Testbeam_Dec_2022/";
  string input2 = input1+runID+"/cluster"+strln;
  TString inp = input2.c_str();
  TrackFit * tf;
  tf= new TrackFit(inp);

  int num = 0,fl = 10000;
  for(auto c:args[0]){
    fl/=10;
    num+= (c-'0')*fl;
  }
  while(true){
    if(num<240){
      align_par = align_par2;
      break;
    }
    if(num<273){
      align_par = align_par1;
      break;
    }
    else{
      align_par = align_par4;
      break;
    }
  }
  tf->Loop(f, myTree,path,args[1]);
  f->Close();
  delete tf; 
  tf = nullptr;
  delete f;
  f = nullptr;
  cout<<"END all"<<endl;
  return 0;
}
